Crawler
========

A fake-3d dungeon crawling game based on the games from long ago
------------------------------------------------------------------

###### Written in Common Lisp (made with alien technology)

### Requirements:
- A common lisp interpreter/compiler
- [Quicklisp](http://www.quicklisp.org)
- lispbuilder sdl
- common sense

### Instructions:
1. load up your favourite lisp (with quicklisp installed)
2. `(ql:quickload "lispbuilder-sdl)`
3. `(load "game.lisp")`
4. __enjoy!__
