(defpackage :game
  (:use :cl ))
(in-package #:game)

(sdl:with-init () 
  (sdl:window 400 300 :title-caption "crawler")
  (setf (sdl:frame-rate) 40)

  (sdl:with-events ()
          (:quit-event () t)

          (:key-down-event ()
           (when (sdl:key-down-p :sdl-key-escape) (sdl:push-quit-event)))

          (:mouse-button-down-event (:button button :x mouse-x :y mouse-y)
           ;; debug code for mouse-click-knowledge-gain
           (format t "~a was pressed at (~a, ~a)~%" button mouse-x mouse-y))

      (:idle ()
       (sdl:with-color (sdl:*red*)
            ;; drawing cross to act as guidelines
            (sdl:draw-line-* 200 150 0 0)
            (sdl:draw-line-* 200 150 0 300)
            (sdl:draw-line-* 200 150 400 0)
            (sdl:draw-line-* 200 150 400 300))
       (sdl:update-display))))
